import { createContext, useState } from "react";

const AuthContenxt = createContext({})

export const AuthProvider = ({ children }) => {
  const [auth, setAuth] = useState({})

  return (
    <AuthContenxt.Provider value={{ auth, setAuth }}>
      {children}
    </AuthContenxt.Provider>
  )
}

export default AuthContenxt