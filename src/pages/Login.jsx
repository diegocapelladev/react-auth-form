import { faCheck, faTimes } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios from "axios"
import { useContext, useEffect, useRef, useState } from "react"
import AuthContenxt from "../context/AuthProvider"

const Login = () => {
  const { setAuth } = useContext(AuthContenxt)
  const userRef = useRef()
  const errRef = useRef()

  const [user, setUser] = useState('')
  const [pwd, setPwd] = useState('')
  const [errMsg, setErrMsg] = useState('')
  const [success, setSuccess] = useState(false)

  useEffect(() => {
    userRef.current.focus()
  }, [])

  useEffect(() => {
    setErrMsg('')
  }, [user, pwd])

  const handleSubmit = async (event) => {
    event.preventDefault()

    try {
      const response = await axios.post('/auth', JSON.stringify({user, pwd}), {
        headers: {
          'Content-Type': 'application/json',
          withCredentials: true
        }
      })

      console.log(JSON.stringify(response?.data))

      // const accessToken = response?.data?.accessToken
      // const roles = response?.data?.roles
      // setAuth({ user, pwd, roles, accessToken })

      setSuccess(true)
    } catch (err) {
      if (!err.response) {
        setErrMsg('No Server Response')
      } else if (err.response?.status === 400) {
        setErrMsg('Misssing Username or Password')
      } else if (err.response?.status === 401) {
        setErrMsg('Unauthorized')
      } else {
        setErrMsg('Login Failed')
      }
      errRef.current.focus()
    }
  }

  return (
    <section>
      <p ref={errRef} className={errMsg ? "errmsg" : "offscreen"} aria-live="assertive">{errMsg}</p>
      <h1>Sign In</h1>

      <form onSubmit={handleSubmit}>
        <label htmlFor="username">
          Username:
        </label>
        <input 
          type="text" 
          id="username" 
          ref={userRef}
          autoComplete="off"
          onChange={(e) => setUser(e.target.value)}
          required
        />

        {/* Password */}
        <label htmlFor="password">
          Password:
        </label>
        <input 
          type="password" 
          id="password" 
          autoComplete="off"
          onChange={(e) => setPwd(e.target.value)}
          required
        />

        <button type="submit">Sign In</button>
      </form>
    </section>
  )
}

export default Login