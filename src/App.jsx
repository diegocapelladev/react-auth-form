import './App.css'
import Login from './pages/Login'
import Register from './pages/Register'

function App() {
  return (
    <div className="App">
      {/* <Register /> */}
      <Login />
    </div>
  )
}

export default App
